FROM alpine

COPY ./Dockerfile ./Dockerfile

COPY --from=golang:alpine /usr/local/go/ /usr/local/go/

ENV PATH="${PATH}:/usr/local/go/bin"
ENV GOBIN="/usr/local/go/bin/"
ENV PYTHONUNBUFFERED=1

RUN apk update --no-cache \
    && \
    apk upgrade --no-cache \
    && \
    apk add --update --no-cache --virtual \
    build-dependencies \
    busybox-extras \
    ca-certificates \
    coreutils \
    curl \
    gcc \
    git \
    glances \
    icu-data-full \
    jq \
    kcat \
    libffi-dev \
    librdkafka \
    librdkafka-dev \
    make \
    musl-dev \
    net-tools \
    nmap-ncat \
    nodejs \
    npm \
    nss \
    openjdk11 \
    openssl-dev \
    python3 \
    redis \
    tcpdump \
    traceroute \
    tzdata \
    unzip \
    vim \
    wget \
    zlib-dev \
    && \
    # ------
    apk add --update --no-cache --virtual \
    bind‑tools \
    build-base \
    && \
    # ------
    apk add --update --no-cache \
    bash

RUN go install github.com/fullstorydev/grpcurl/cmd/grpcurl@latest

RUN python -m ensurepip \
    && pip3 install --no-cache --upgrade pip setuptools

RUN printf "\nVersions:\n \
\t- Alpine Linux $(cat /etc/alpine-release) ($(uname -r))\n \
\t- Golang: $(go version)\n \
\t- Java: $(java --version  | sed -e 1q)\n \
\t- Python: $(python --version)\n \
\t- NodeJS:  $(node --version)\n \
\t- NPM: $(npm --version)\n \
\n"
